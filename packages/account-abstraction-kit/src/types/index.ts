import { RelayPack } from 'ultron-foundation-relay-kit'

export enum OperationType {
  Call,
  DelegateCall
}

export interface AccountAbstractionConfig {
  relayPack: RelayPack
}
