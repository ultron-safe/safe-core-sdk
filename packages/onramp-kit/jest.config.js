const config = {
  testEnvironment: 'jsdom',
  collectCoverageFrom: ['src/**/*.{ts,tsx}'],
  roots: ['<rootDir>/src'],
  verbose: true,
  transform: {
    '^.+\\.ts?$': 'ts-jest'
  },
  moduleNameMapper: {
    '^ultron-foundation-protocol-kit/typechain/(.*)$': '<rootDir>/../protocol-kit/typechain/$1',
    '^ultron-foundation-protocol-kit/(.*)$': '<rootDir>/../protocol-kit/src/$1',
    '^ultron-foundation-onramp-kit/(.*)$': '<rootDir>/src/$1'
  }
}

module.exports = config
